package com.kztma.delivering.controller;

import com.kztma.delivering.domain.User;
import com.kztma.delivering.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by cccc on 11/19/2017.
 */

@RestController
@RequestMapping("/register")
public class SignUpController {

    @Autowired
    private UserService userService;


    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    User createUser(@Valid @RequestBody User user){
        return userService.register(user);
    }
}
