package com.kztma.delivering.controller;

import com.kztma.delivering.domain.User;
import com.kztma.delivering.pojo.RoleName;
import com.kztma.delivering.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cccc on 11/18/2017.
 */

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    /*@RequestMapping(value ="/cities")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
    public List<User> getUsers(){
        return userService.findAll();
    }*/

    @RequestMapping(value="roles", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('DISPATCHER')")
    public List<RoleName> getRoles(){
        return Arrays.stream(RoleName.values()).collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('CONTROLLER') or hasAuthority('DELIVERYMAN') or hasAuthority('DISPATCHER') or hasAuthority('OPERATOR')")
    public List<User> getUsers(){
        return (List<User>) userService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN')")
    public @ResponseBody User createUser(@Valid @RequestBody User user){
        return userService.create(user);
    }
}
