package com.kztma.delivering.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Created by cccc on 11/19/2017.
 */

@ControllerAdvice
public class GenericExceptionHandler {

    @Autowired
    MessageSource messageSource;

    @ResponseBody
    @ExceptionHandler(DataIntegrityViolationException.class)
    ResponseEntity<?> handleConflict(DataIntegrityViolationException e) {
        return response(HttpStatus.CONFLICT, 40000, "Operation cannot be performed. Integrity Constraint violated", "");
    }



    /**
     * For handling validation errors
     */
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<RestError> invalidInput(MethodArgumentNotValidException ex) {
        return response(BAD_REQUEST, 40001, "Validation Error", ex.getMessage());
    }

    private ResponseEntity<RestError> response(HttpStatus status, int code, String msg) {
        return response(status, code, msg, "");
    }

    private ResponseEntity<RestError> response(HttpStatus status, int code, String msg, String moreInfo) {
        return new ResponseEntity<>(new RestError(status.value(), code, msg, moreInfo), status);
    }
}
