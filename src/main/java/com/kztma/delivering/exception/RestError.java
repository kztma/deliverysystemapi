package com.kztma.delivering.exception;

/**
 * Created by cccc on 11/19/2017.
 */
public class RestError {

    private int httpStatusCode;
    private int code;
    private String message;
    private String moreInfo;

    public RestError(int value, int code, String message, String moreInfo) {
        this.httpStatusCode = value;
        this.code = code;
        this.message = message;
        this.moreInfo = moreInfo;
    }

    public RestError() {
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RestError)) return false;

        RestError restError = (RestError) o;

        if (getHttpStatusCode() != restError.getHttpStatusCode()) return false;
        if (getCode() != restError.getCode()) return false;
        if (getMessage() != null ? !getMessage().equals(restError.getMessage()) : restError.getMessage() != null)
            return false;
        return getMoreInfo() != null ? getMoreInfo().equals(restError.getMoreInfo()) : restError.getMoreInfo() == null;
    }

    @Override
    public int hashCode() {
        int result = getHttpStatusCode();
        result = 31 * result + getCode();
        result = 31 * result + (getMessage() != null ? getMessage().hashCode() : 0);
        result = 31 * result + (getMoreInfo() != null ? getMoreInfo().hashCode() : 0);
        return result;
    }
}
