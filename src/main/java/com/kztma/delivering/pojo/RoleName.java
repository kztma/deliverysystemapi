package com.kztma.delivering.pojo;

/**
 * Created by cccc on 11/18/2017.
 */
public enum RoleName {

    ADMIN("ADMIN"),
    CONTROLLER("CONTROLLER"),
    DELIVERYMAN("DELIVERYMAN"),
    DISPATCHER("DISPATCHER"),
    OPERATOR("OPERATOR"),
    USER("USER");

    private final String value;

    RoleName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleName fromValue(String v) {
        for (RoleName roleName : RoleName.values()) {
            if (roleName.value.equals(v)) {
                return roleName;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
