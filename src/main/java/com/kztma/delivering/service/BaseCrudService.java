package com.kztma.delivering.service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by cccc on 11/19/2017.
 */
public abstract class BaseCrudService<T, ID extends Serializable> {

    public abstract JpaRepository<T, ID> getRepo();


    public Collection<T> findAll() {
        return getRepo().findAll();
    }

    public T findOne(ID id) {
        return getRepo().findOne(id);
    }

    public T save(T model) {
        return getRepo().save(model);
    }

    public T create(T model) {
        return save(model);
    }

    public T update(T model) {
        return save(model);
    }

    public void delete(ID id) {
        getRepo().delete(id);
    }
}
