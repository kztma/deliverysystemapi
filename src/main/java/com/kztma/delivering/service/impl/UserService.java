package com.kztma.delivering.service.impl;

import com.kztma.delivering.domain.Role;
import com.kztma.delivering.domain.User;
import com.kztma.delivering.repository.RoleRepository;
import com.kztma.delivering.repository.UserRepository;
import com.kztma.delivering.service.BaseCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static com.kztma.delivering.pojo.RoleName.USER;
import static com.kztma.delivering.util.SecurityUtil.sha256Decode;

/**
 * Created by cccc on 11/18/2017.
 */

@Service
public class UserService extends BaseCrudService<User, Long> {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public JpaRepository<User, Long> getRepo() {
        return this.userRepository;
    }

    @Override
    public User save(User model) {
        model.setPassword(sha256Decode(model.getPassword()));
        return super.save(model);
    }

    public User register(User user) {
        Role userRole = roleRepository.findRoleByRoleName(USER);
        List<Role> roles = Arrays.asList(userRole);
        user.setRoles(roles);
        return this.create(user);
    }
}
